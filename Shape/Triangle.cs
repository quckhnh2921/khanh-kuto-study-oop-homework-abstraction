﻿namespace Shape_Abstraction
{
    public class Triangle : Shape
    {
        public override double CaculateArea(double x, double y) => Math.Round((x * y) / 2,2);
    }
}

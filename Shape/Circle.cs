﻿namespace Shape_Abstraction
{
    public class Circle : Shape
    {
        public override double CaculateArea(double x, double y) => Math.Round((x * y) * Math.PI,2);
    }
}

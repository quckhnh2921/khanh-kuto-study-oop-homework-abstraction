﻿namespace Shape_Interface
{
    public class Rectangle : Shape
    {
        public double CaculateArea(double x, double y) => Math.Round(x * y, 2);
    }
}

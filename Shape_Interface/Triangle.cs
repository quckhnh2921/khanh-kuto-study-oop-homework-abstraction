﻿namespace Shape_Interface
{
    public class Triangle : Shape
    {
        public double CaculateArea(double x, double y) => Math.Round((x * y) / 2, 2);
    }
}
